package com.bti360.users;

public enum UserRole {
    USER,
    ADMIN;
}
