package com.bti360.users;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

/**
 * Created by gshankman on 5/21/17.
 */
public class X509HeaderFilter implements Filter {
    private static final Logger LOG = LoggerFactory.getLogger(X509HeaderFilter.class);

    private static final String X509_ATTRIBUTE = "javax.servlet.request.X509Certificate";

    private static final String SSL_VERIFIED = "x-ssl-verified";
    private static final String SSL_DN = "x-ssl-dn";
    private static final String SSL_CERT = "x-ssl-cert";

    private static final String VALID_VERIFICATION = "SUCCESS";

    private CertificateFactory x509Factory;

    public X509HeaderFilter() throws CertificateException {
        x509Factory = CertificateFactory.getInstance("X.509");
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // do nothing
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest)request;
        if (noCertificateInRequest(httpRequest) && hasCertificateHeaders(httpRequest)) {
            X509Certificate cert = extractClientCertificate(httpRequest);
            if (cert != null) {
                request.setAttribute(X509_ATTRIBUTE, new X509Certificate[] { cert });
            }
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        // do nothing
    }

    private boolean hasCertificateHeaders(HttpServletRequest request) {
        String sslVerified = request.getHeader(SSL_VERIFIED);
        String sslDn = request.getHeader(SSL_DN);
        String sslCert = request.getHeader(SSL_CERT);

        if (!VALID_VERIFICATION.equals(sslVerified)) {
            LOG.debug("X.509 Verification failed. " + SSL_VERIFIED + "=" + sslVerified);
            return false;
        }

        if (sslDn == null || sslDn.trim().isEmpty()) {
            LOG.debug(SSL_DN + " not found.");
            return false;
        }

        if (sslCert == null || sslCert.trim().isEmpty()) {
            LOG.debug("No client certificate found in request.");
            return false;
        }

        return true;
    }

    private boolean noCertificateInRequest(HttpServletRequest request) {
        X509Certificate[] certs = (X509Certificate[]) request.getAttribute(X509_ATTRIBUTE);
        return certs == null || certs.length == 0;
    }

    private X509Certificate extractClientCertificate(HttpServletRequest request) {
        String sslCert = request.getHeader(SSL_CERT);
        InputStream certIn = new ByteArrayInputStream(sslCert.replaceAll("\t", "\n").getBytes());
        try {
            X509Certificate cert = (X509Certificate)x509Factory.generateCertificate(certIn);
            LOG.debug("Found X.509 Client Certificate: " + cert);
            return cert;
        } catch (CertificateException e) {
            LOG.debug("Invalid X.509 Client Certificate: " + e, e);
            return null;
        }
    }
}
