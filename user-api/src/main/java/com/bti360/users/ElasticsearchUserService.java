package com.bti360.users;

import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Map;

@Component
public class ElasticsearchUserService implements UserService {
    private static final String USER_INDEX = "random-users";
    private static final String USER_TYPE = "User";

    private static final String[] USER_PROPERTY_EXCLUDES = new String[]{
            "large_image",
            "large_image_type",
            "medium_image",
            "medium_image_type",
            "thumbnail_image",
            "thumbnail_image_type",
            "password"
    };

    @Autowired
    private Client elasticsearch;

    @Override
    public Map<String, Object> getUserByUsername(String username) {
        return getUser(username, USER_PROPERTY_EXCLUDES);
    }

    private Map<String, Object> getUser(String username, String... excludes) {
        GetResponse response = elasticsearch.prepareGet(USER_INDEX, USER_TYPE, username)
                .setFetchSource(null, excludes)
                .get();
        return response.getSourceAsMap();
    }

    @Override
    public byte[] getProfilePicture(String username, ImageSize imageSize, String imageFormat) throws IOException {
        Map user = getUser(username);
        String imageStr;
        switch (imageSize) {
            case LARGE:
                imageStr = (String) user.get("large_image");
                break;
            case MEDIUM:
                imageStr = (String) user.get("medium_image");
                break;
            case THUMBNAIL:
            default:
                imageStr = (String) user.get("thumbnail_image");
                break;
        }
        return decodeImage(imageStr, imageFormat);
    }

    @Override
    public List<Map<String, Object>> searchUsers(String search) {
        QueryBuilder query;
        if (search != null && !search.trim().isEmpty()) {
            query = QueryBuilders.matchQuery("_all", search);
        } else {
            query = QueryBuilders.matchAllQuery();
        }

        List<Map<String, Object>> users = new ArrayList<>();
        for (SearchHit hit : doScroll(query, USER_PROPERTY_EXCLUDES)) {
            users.add(hit.getSource());
        }
        users.sort((o1, o2) -> {
            String name1 = (String)o1.getOrDefault("full_name", "");
            String name2 = (String)o2.getOrDefault("full_name", "");
            return name1.compareTo(name2);
        });
        return users;
    }

    private List<SearchHit> doScroll(QueryBuilder query, String... excludes) {
        List<SearchHit> hits = new ArrayList<>();
        SearchResponse scrollResponse = elasticsearch.prepareSearch("random-users")
                .setTypes("User")
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                .setQuery(query)
                .setSize(100)
                .setScroll(new TimeValue(60000))
                .setFetchSource(null, excludes)
                .get();

        do {
            hits.addAll(Arrays.asList(scrollResponse.getHits().getHits()));
            scrollResponse = elasticsearch.prepareSearchScroll(scrollResponse.getScrollId())
                    .setScroll(new TimeValue(60000))
                    .execute()
                    .actionGet();
        } while (scrollResponse.getHits().getHits().length != 0);
        return hits;
    }

    private byte[] decodeImage(String base64Image, String targetFormat) throws IOException {
        Base64.Decoder decoder = Base64.getDecoder();
        try (InputStream imageIn = decoder.wrap(new ByteArrayInputStream(base64Image.getBytes("UTF-8")))) {
            BufferedImage image = ImageIO.read(imageIn);
            ByteArrayOutputStream imageOut = new ByteArrayOutputStream();
            ImageIO.write(image, targetFormat, imageOut);
            return imageOut.toByteArray();
        }
    }
}
