package com.bti360.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * API Controller for Users
 */
@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private UserRoleService userRoleService;

    @PreAuthorize("hasAuthority('ROLE_USER')")
    @RequestMapping("/whoami")
    public Map getCurrentUser(Principal user) {
        return getFullyPopulatedUser(user.getName());
    }

    @PreAuthorize("hasAuthority('ROLE_USER')")
    @RequestMapping("/whoami/roles")
    public List<String> getCurrentUserRoles(Authentication authentication) {
        return authentication.getAuthorities().
                stream().
                map(GrantedAuthority::getAuthority).
                collect(Collectors.toList());
    }

    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @RequestMapping("/users/{username}")
    public Map getUserByUsername(@PathVariable("username") String username) {
        return getFullyPopulatedUser(username);
    }

    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @RequestMapping("/users")
    public List<Map<String, Object>> searchUsers(@RequestParam(value = "q", required = false) String search) {
        return userService.searchUsers(search).
                stream().
                map(this::updateUserProperties).
                collect(Collectors.toList());
    }

    private Map<String, Object> getFullyPopulatedUser(String username) {
        return updateUserProperties(userService.getUserByUsername(username));
    }

    private Map<String, Object> updateUserProperties(Map<String, Object> user) {
        // Uncomment the line below to add an 'isAdmin' property for the incoming user
//        user.put("isAdmin", isAdmin((String)user.get("username")));
        return user;
    }

    private boolean isAdmin(String username) {
        return userRoleService.getRolesForUser(username).contains(UserRole.ADMIN);
    }
}
