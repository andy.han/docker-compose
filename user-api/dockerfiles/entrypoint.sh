#!/bin/sh

API_HOME=/user-api

case "$1" in
    run)
        cd ${API_HOME}
        if [ "${DEBUG_API}" == "true" ]; then
            ./gradlew bootRun --debug-jvm
        else
            ./gradlew bootRun
        fi
        ;;
    *)
        $*
esac
