from pybuilder.core import init, use_plugin

use_plugin('python.core')
use_plugin('python.install_dependencies')
use_plugin('filter_resources')
use_plugin('python.distutils')

name = 'gen-users'
version = '1.0.0.dev'
default_task = 'publish'


@init
def initialize(project):
    project.depends_on('click')
    project.depends_on('elasticsearch', '~=5.0')
    project.depends_on('requests')
    project.depends_on('pyldap')

    project.build_depends_on('flake8')
    project.build_depends_on('mock')

    project.set_property('distutils_console_scripts', [
        'gen-users = gen_users:cli'
    ])

    project.set_property('filter_resources_glob', [
        '**/gen_users/__init__.py',
        '**/gen_users/_cli.py'
    ])

    project.include_file('gen_users', '*.json')
    project.include_file('gen_users.resources', '*.png')

    project.set_property('coverage_break_build', False)
    project.set_property('coverage_exceptions', [
        'gen_users.__main__'
    ])

    project.set_property('distutils_commands', ['sdist'])
