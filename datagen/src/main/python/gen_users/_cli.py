import codecs
import json

import logging
import os
from click import command, option, argument, Path, version_option, Choice

from ._ldap import LdapHelper
from ._es import EsHelper
from ._user import generate_users

LOG_LEVELS = ['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL', 'FATAL', 'NOTSET']


def _run(num_users, seed, es_host,
         ldap_url, ldap_user, ldap_password, user_base_dn, admin_group_dn, user_group_dn,
         log_level, output_path):
    logging.basicConfig()
    log = logging.getLogger('datagen')
    log.setLevel(log_level)

    users = generate_users(num_users, seed)

    users_json = os.path.join(output_path, 'users.json')
    # users_ldif = os.path.join(output_path, 'users.ldif')

    log.info("Writing users to %s", users_json)
    with open(users_json, 'w') as f:
        f.write(json.dumps([u.json for u in users], indent=2))

    # log.info("Writing LDIF to %s", users_ldif)
    # with codecs.open(users_ldif, 'w', 'utf-8') as f:
    #     for user in users:
    #         for k, v in user.ldif:
    #             f.write('%s: %s\n' % (k, v))
    #         f.write('\n')

    if es_host:
        EsHelper(es_host, users).populate_user_index()

    if ldap_url:
        random_users = [x for x in users if x.uid not in ['btiuser', 'btiadmin']]
        LdapHelper(ldap_url=ldap_url,
                   bind_user=ldap_user,
                   bind_password=ldap_password,
                   user_base_dn=user_base_dn,
                   admin_group_dn=admin_group_dn,
                   user_group_dn=user_group_dn).add_users(random_users)

    log.info("Completed User Generation")


@command()
@version_option(version='${version}')
@option('-n', '--num-users', 'num_users', type=int, default=10, show_default=True, metavar='N', envvar='NUM_USERS',
        help='The number of users to create.')
@option('-s', '--seed', 'seed', type=str, default='bti360docker', metavar='<seed>', envvar='USER_SEED',
        help='The seed for the random user generator.')
@option('-e', '--es-host', 'es_host', type=str, metavar='<http[s]://es[:9200]>', envvar='ES_HOST',
        help='The URL to connect to Elasticsearch.')
@option('-d', '--directory', 'ldap_url', type=str, metavar='<ldap://ldap:389>', envvar='LDAP_URL',
        help='The URL for the LDAP server')
@option('-u', '--ldap-user', 'ldap_user', type=str, metavar='<bind_user>', envvar='LDAP_BIND_USER',
        help='The username or DN to bind to the LDAP server')
@option('-p', '--ldap-password', 'ldap_password', type=str, metavar='<bind_password>', envvar='LDAP_BIND_PASSWORD',
        help='The password to bind to the LDAP server')
@option('--user-base-dn', 'user_base_dn', type=str, metavar='<DN>', envvar='USER_BASE_DN',
        help='The base DN for users.')
@option('--admin-group-dn', 'admin_group_dn', type=str, metavar='<DN>', envvar='ADMIN_GROUP_DN',
        help='The admin group DN.')
@option('--user-group-dn', 'user_group_dn', type=str, metavar='<DN>', envvar='USER_GROUP_DN',
        help='The user group DN.')
@option('-l', '--log-level', 'log_level', type=Choice(choices=LOG_LEVELS), default='INFO', show_default=True,
        metavar='<level>', envvar='LOG_LEVEL',
        help='The log level for the application.')
@argument('output_path', type=Path(exists=True, file_okay=False, dir_okay=True), default='.')
def cli(num_users, seed, es_host,
        ldap_url, ldap_user, ldap_password, user_base_dn, admin_group_dn, user_group_dn,
        log_level, output_path):
    """
    Creates an arbitrary number of random users.
    """
    _run(num_users, seed,
         es_host,
         ldap_url, ldap_user, ldap_password,
         user_base_dn, admin_group_dn, user_group_dn,
         log_level, output_path)
