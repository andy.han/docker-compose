import hashlib
import json

import os
from base64 import b64encode, encodestring, decodestring

import logging

import re
import requests

_log = logging.getLogger('datagen')

_RANDOM_USER_API = 'https://randomuser.me/api'


def generate_users(num_users, seed):
    _log.info('Generating %d users. (seed: %s)', num_users, seed)
    params = dict(results=num_users,
                  nat='us,ca',
                  seed=seed)
    _log.debug("URL: %s, Params: %s", _RANDOM_USER_API, params)
    response = requests.get('https://randomuser.me/api', params=params).json()
    _log.debug("API Response: %s", response)

    return _load_canned_users() + [RandomUser(x, is_admin=(i % 12 == 0)) for i, x in enumerate(response['results'])]


def _load_canned_users():
    my_dir = os.path.dirname(__file__)
    with open(os.path.join(my_dir, 'canned-users.json')) as f:
        return [RandomUser(x) for x in json.loads(f.read().replace('${script_path}', my_dir))]


class RandomUser(object):
    def __init__(self, user, is_admin=False):
        """
        :param user: the user dict
        :type user: dict
        :param is_admin: True if the user is an admin
        :type is_admin: bool
        """
        self._user = user
        self.is_admin = is_admin

        self._large_image = None
        self._medium_image = None
        self._thumbnail_image = None

    @property
    def json(self):
        """
        :return: the original JSON object
        :rtype: dict
        """
        return self._user

    @property
    def es(self):
        """
        :return: an Elasticsearch User document
        :rtype: dict
        """
        return dict(full_name=self.name,
                    title=self.title,
                    first_name=self.given_name,
                    last_name=self.surname,
                    birth_date=self.birth_date,
                    gender=self.gender,
                    nationality=self.country,
                    id_type=self._user['id']['name'],
                    id_value=self._user['id']['value'],
                    cell_phone=self.mobile_phone,
                    phone=self.home_phone,
                    address=self.full_address,
                    street=self.street,
                    city=self.city,
                    state=self.state,
                    postcode=self.zip_code,
                    registration_date=self.registration_date,
                    username=self.uid,
                    password=self.password,
                    email=self.email,
                    large_image=self.large_image,
                    large_image_type=self.large_image_type,
                    medium_image=self.medium_image,
                    medium_image_type=self.medium_image_type,
                    thumbnail_image=self.thumbnail_image,
                    thumbnail_image_type=self.thumbnail_image_type)

    @property
    def ldif(self):
        return [
            ('dn', self.dn),
            ('objectClass', 'inetOrgPerson'),
            ('objectClass', 'organizationalPerson'),
            ('objectClass', 'person'),
            ('objectClass', 'top'),
            ('cn', self.uid),
            ('title', self.title),
            ('sn', self.surname),
            ('gn', self.given_name),
            ('displayName', self.name),
            ('photo', self.photo),
            ('street', self.street),
            ('stateOrProvinceName', self.state),
            ('localityName', '%s, %s' % (self.city, self.country)),
            ('postalCode', self.zip_code),
            ('homeTelephoneNumber', self.home_phone),
            ('mobileTelephoneNumber', self.mobile_phone),
            ('mail', self.email),
            ('uid', self.uid)
        ]

    @property
    def dn(self):
        return 'cn=%s,ou=users,dc=demo,dc=bti360,dc=com' % self.uid

    @property
    def name(self):
        return '%s %s' % (self.given_name, self.surname)

    @property
    def title(self):
        return self._user['name']['title'].capitalize()

    @property
    def given_name(self):
        return self._user['name']['first'].capitalize()

    @property
    def surname(self):
        return self._user['name']['last'].capitalize()

    @property
    def email(self):
        return self._user['email']

    @property
    def birth_date(self):
        return self._user['dob']

    @property
    def registration_date(self):
        return self._user['registered']

    @property
    def gender(self):
        return self._user['gender']

    @property
    def street(self):
        return self._user['location']['street'].title()

    @property
    def city(self):
        return self._user['location']['city'].title()

    @property
    def state(self):
        return self._user['location']['state'].title()

    @property
    def country(self):
        return self._user['nat'].upper()

    @property
    def zip_code(self):
        return str(self._user['location']['postcode'])

    @property
    def full_address(self):
        return '%s, %s, %s %s, %s' % (self.street, self.city, self.state, self.zip_code, self.country)

    @property
    def uid(self):
        return self._user['login']['username']

    @property
    def password(self):
        return self._user['login']['password']

    @property
    def password_salt(self):
        return decodestring(self._user['login']['salt'])

    @property
    def password_sha256(self):
        h = hashlib.sha256(self.password)
        h.update(self.password_salt)
        return '{SSHA-256}%s' % encodestring(h.digest() + self.password_salt)

    @property
    def home_phone(self):
        return self._user['phone']

    @property
    def mobile_phone(self):
        return self._user['cell']

    @property
    def photo(self):
        return self.large_image

    @property
    def large_image(self):
        if self._large_image is None:
            self._large_image = _load_image_b64(self._user['picture']['large'])
        return self._large_image

    @property
    def medium_image(self):
        if self._medium_image is None:
            self._medium_image = _load_image_b64(self._user['picture']['medium'])
        return self._medium_image

    @property
    def thumbnail_image(self):
        if self._thumbnail_image is None:
            self._thumbnail_image = _load_image_b64(self._user['picture']['thumbnail'])
        return self._thumbnail_image

    @property
    def large_image_type(self):
        return _get_image_type(self._user['picture']['large'])

    @property
    def medium_image_type(self):
        return _get_image_type(self._user['picture']['medium'])

    @property
    def thumbnail_image_type(self):
        return _get_image_type(self._user['picture']['thumbnail'])


def _load_image_b64(url):
    """
    Reads an image from the target URL and Base64 encodes it.
    
    :param url: the URL
    :type url: str
    
    :return: a Base64 encoded string representing the image
    :rtype: str
    """
    file_match = re.match(r'^file://(.+)', url, re.IGNORECASE)
    if file_match:
        image_bytes = _load_image_from_file(file_match.group(1))
    else:
        image_bytes = requests.get(url).content
    return b64encode(image_bytes)


def _load_image_from_file(path):
    """
    Reads an image from a local file.

    :param path: the file path
    :type path: str

    :return: the bytes of the file
    :rtype: bytearray
    """
    with open(path) as f:
        return f.read()


def _get_image_type(url, default='png'):
    """
    Searches for the image type in the URL and returns the first match
    for png, jpg, or gif.

    :param url: the image url
    :type url: str
    :param default: the default value if the file type cannot be determined
    :type default: str

    :return: the discovered file type
    :rtype: str
    """
    m = re.match(r'\.(jpg|png|gif)(?:[^/?].*)?$', url, re.IGNORECASE)
    return m.group(1) if m else default
