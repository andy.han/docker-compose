#!/bin/sh

case "$1" in
    gen-users)
        TARGET_DIR=$(test -n "$2" && echo $2 || echo "/random_users")
        if [ -n ${ES_HOST} ]; then
            /wait-for-it.sh ${ES_HOST} -s -t ${SERVICE_TIMEOUT:-30} || exit 1
        fi

        if [ -n ${LDAP_URL} ]; then
            CLEAN_LDAP_URL=$(echo ${LDAP_URL} | sed -E 's_^ldaps?://__')
            /wait-for-it.sh ${CLEAN_LDAP_URL} -s -t ${SERVICE_TIMEOUT:-30} || exit 1
        fi

        gen-users ${TARGET_DIR}
        ;;
    bg)
        yes >/dev/null
        ;;
    *)
        $*
esac
