import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MdListModule, MdButtonModule, MdIconModule, MdInputModule } from '@angular/material';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminPageComponent } from './admin-page/admin-page.component';

@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    MdListModule,
    MdButtonModule,
    MdIconModule,
    MdInputModule
  ],
  declarations: [AdminPageComponent]
})
export class AdminModule { }
