import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'app/store';
import { Observable } from 'rxjs/Observable';

import { getUsersState } from 'app/store/selectors';
import { User } from 'app/store/models';
import {LoadUsersAction} from "../../store/users/actions";
import {Router} from "@angular/router";

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.css']
})
export class AdminPageComponent {
  public users$: Observable<User[]>;
  public query: string;

  constructor(private store: Store<AppState>, private router: Router) {
    this.users$ = this.store.select(getUsersState);
    this.store.dispatch(new LoadUsersAction());
  }

  onSelect(user: User) {
    this.router.navigate(['/profile', user.username]);
  }
}
