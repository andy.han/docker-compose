import { Injectable } from '@angular/core';
import { UserService } from 'app/shared/services';
import { Store } from '@ngrx/store';
import { AppState } from 'app/store/';
import { User } from 'app/store/models';

import 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import {LoadMeSuccessAction} from 'app/store/actions';


@Injectable()
export class StartupService {

  private me: User;

  constructor(private store: Store<AppState>, private userService: UserService) { }

  public load(): Promise<any> {
    return this.userService.getMe()
      .map(me => this.me = me)
      .switchMap(() => this.userService.getMyRoles())
      .map(roles => {
        this.me.type = 'me';
        this.me.roles = roles;
      })
      .toPromise()
      .then(() => this.store.dispatch(new LoadMeSuccessAction(this.me)))
      .catch((err: any) => Promise.resolve());
  }
}
