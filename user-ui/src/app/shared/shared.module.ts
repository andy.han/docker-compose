import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StartupService } from './services/startup/startup.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [StartupService]
})
export class SharedModule { }
