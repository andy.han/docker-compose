import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {APP_INITIALIZER, NgModule} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { RouterStoreModule } from '@ngrx/router-store';
import { EffectsModule } from '@ngrx/effects';
import { MdSidenavModule, MdButtonModule } from '@angular/material';

import { SharedModule } from './shared/shared.module';
import { AppReducers } from './store';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { UserService } from './shared/services';
import { UserEffects, ProfileEffects } from './store/effects';
import {StartupService} from "./shared/services/startup/startup.service";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    MdSidenavModule,
    MdButtonModule,

    AppRoutingModule,
    StoreModule.provideStore(AppReducers),
    RouterStoreModule.connectRouter(),
    StoreDevtoolsModule.instrumentOnlyWithExtension(),
    EffectsModule.run(UserEffects),
    EffectsModule.run(ProfileEffects),

    SharedModule
  ],
  providers: [
    UserService,
    {
      provide: APP_INITIALIZER,
      useFactory: startupServiceFactory,
      deps: [StartupService],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function startupServiceFactory(startupService: StartupService): Function {
  return () => startupService.load();
}
