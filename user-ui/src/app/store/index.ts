import { routerReducer, RouterState } from '@ngrx/router-store';

import { UsersState, MeState, ProfileState } from './models';
import { usersReducer, meReducer, profileReducer } from './reducers';

export const AppReducers = {
  router: routerReducer,
  users: usersReducer,
  me: meReducer,
  profile: profileReducer
};

export interface AppState {
  router: RouterState;
  users: UsersState;
  me: MeState;
  profile: ProfileState;
}
