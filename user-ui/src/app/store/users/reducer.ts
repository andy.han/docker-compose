import { Action } from '@ngrx/store';

import {
  LOAD_USERS_SUCCESS,
} from './actions';
import { UsersState, initialUsersState } from './model';

export function usersReducer(state: UsersState = initialUsersState, action: Action): UsersState {
  switch (action.type) {
    case LOAD_USERS_SUCCESS:
      return action.payload;
    default:
      return state;
  }
}
