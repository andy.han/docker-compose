import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/switchMap';

import { UserService } from 'app/shared/services';
import {
  LOAD_USERS,
  LoadUsersErrorAction,
  LoadUsersSuccessAction,
 } from './actions';

@Injectable()
export class UserEffects {

    constructor (private update$: Actions, private userService: UserService) {}

    @Effect() loadUsers$: Observable<Action> = this.update$
        .ofType(LOAD_USERS)
        .switchMap(() => this.userService.getUsers())
        .map(users => new LoadUsersSuccessAction(users))
        .catch((err: any, caught: any) => Observable.of(new LoadUsersErrorAction()));
}
