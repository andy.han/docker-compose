import { Observable } from 'rxjs/Observable';

import * as _ from 'lodash';
import 'rxjs/add/operator/map';

import { AppState } from '../';
import { User } from 'app/store/models';
import { UsersState } from './model';

export const getUsersState = (state: AppState) => state.users;

export function getUser(id) {
  return ((users$: Observable<UsersState>) => {
    return users$.map((users: User[]) => {
      return _.find(users, {id: +id});
    });
  });
}
