import { Action } from '@ngrx/store';

import {
  LOAD_PROFILE_SUCCESS,
} from './actions';
import { ProfileState, initialProfileState } from './model';

export function profileReducer(state: ProfileState = initialProfileState, action: Action): ProfileState {
  switch (action.type) {
    case LOAD_PROFILE_SUCCESS:
      return action.payload;
    default:
      return state;
  }
}
