import {Injectable} from "@angular/core";
import {Actions, Effect} from "@ngrx/effects";
import {Action} from "@ngrx/store";
import {Observable} from "rxjs/Observable";

import "rxjs/add/operator/catch";
import "rxjs/add/operator/switchMap";

import {UserService} from "app/shared/services";
import {LOAD_PROFILE, LoadProfileAction, LoadProfileErrorAction, LoadProfileSuccessAction} from "./actions";

@Injectable()
export class ProfileEffects {

    constructor (private update$: Actions, private userService: UserService) {}

    @Effect() loadProfile$: Observable<Action> = this.update$
        .ofType(LOAD_PROFILE)
        .switchMap((action: LoadProfileAction) => this.userService.getUser(action.payload))
        .map(user => new LoadProfileSuccessAction(user))
        .catch((err: any, caught: any) => Observable.of(new LoadProfileErrorAction()));
}
