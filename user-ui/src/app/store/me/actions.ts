import { Action } from '@ngrx/store';

import { User } from 'app/store/models';

export const LOAD_ME_ERROR = '[Me] Load Me Error';
export const LOAD_ME_SUCCESS = '[Me] Load Me Success';

export class LoadMeErrorAction implements Action {
  readonly type = LOAD_ME_ERROR;
  readonly payload = {};
};

export class LoadMeSuccessAction implements Action {
  readonly type = LOAD_ME_SUCCESS;
  constructor(public payload: User) { }
};
