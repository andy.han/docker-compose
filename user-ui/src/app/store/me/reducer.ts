import { Action } from '@ngrx/store';

import {
  LOAD_ME_SUCCESS,
} from './actions';
import { MeState, initialMeState } from './model';

export function meReducer(state: MeState = initialMeState, action: Action): MeState {
  switch (action.type) {
    case LOAD_ME_SUCCESS:
      return action.payload;
    default:
      return state;
  }
}
