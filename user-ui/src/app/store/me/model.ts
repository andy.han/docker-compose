import { User } from 'app/store/models';

export type MeState = User;

export const initialMeState: User = {
  address: '',
  cell_phone: '',
  city: '',
  dob: new Date(),
  email: '',
  first_name: '',
  full_name: '',
  gender: '',
  id_type: '',
  id_value: '',
  last_name: '',
  nationality: '',
  phone: '',
  postcode: '',
  registration_date: new Date(),
  roles: [],
  state: '',
  street: '',
  title: '',
  type: '',
  username: ''
}
