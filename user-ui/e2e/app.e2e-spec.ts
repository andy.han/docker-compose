import { DockerProfilePage } from './app.po';

describe('docker-profile App', () => {
  let page: DockerProfilePage;

  beforeEach(() => {
    page = new DockerProfilePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
