#!/bin/sh

docker pull python:2-alpine
docker pull dinkel/openldap
docker pull nginx:1.13-alpine
docker pull openjdk:8-alpine
docker pull node:7-alpine
docker pull docker.elastic.co/elasticsearch/elasticsearch:5.4.0

docker build -t users:api user-api
docker build -t users:ui user-ui
docker build -t users:proxy nginx
docker build -t users:ldap ldap
docker build -t users:datagen datagen
