# Docker Compose Kata - June 2017

Introduction to Docker Compose for configuring development environments.

Follow the [Kata Preparation](https://gitlab.com/bti360-kata/docker-compose/wikis/Kata-Prep)
instructions to get your environment configured for the kata.
