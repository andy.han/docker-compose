## Docker Run

Let's see how easy it is to spin up an Elasticsearch server where we can store our user profiles:

```bash
docker run --rm \
           -p 9200:9200 \
           -e cluster.name=docker-kata \
           -e transport.host=0.0.0.0 \
           -e http.host=0.0.0.0 \
           -e bootstrap.memory_lock=true \
           -e ES_JAVA_OPTS="-Xms512m -Xmx512m" \
           -e xpack.security.enabled=false \
           --ulimit memlock=-1:-1 \
           docker.elastic.co/elasticsearch/elasticsearch:5.4.0
```

Just like that, we've got Elasticsearch up and running, but trying to remember all those options or share
them across a team is pretty difficult.

## Docker Compose

That's where `docker-compose` comes in. Compose is a utility that lets you define a set of services and all
their configuration options and manage them with simple commands.

Let's look at a simple compose file that spins up our Elasticsearch service.

**_<https://gitlab.com/bti360-kata/docker-compose/blob/master/.kata/docker-compose.1.yml>_**

Each `service` in a compose file is a hash of configuration settings that define a Docker container. The
configuration keys correspond to the arguments to `docker run`.

Here, you can see we're defining a single service, `es`, that creates our Elasticsearch container. We can
add the compose file to version control and anyone checking out the project can use the exact same
environment for development and testing!

Let's use `docker-compose up` to start our service:

```bash
docker-compose -f docker-compose.1.yml up
```

Now that we've got Elasticsearch up, let's add some sample data:

```bash
curl -XPOST localhost:9200/users/User/1 -d '{"name": "Gordon", "likes": "Docker"}'
curl -s localhost:9200/users/User/1 | jq
```

You can see I now have a record in the `users` index. What happens if we need to recreate the container? Let's
move Elasticsearch to a different port.

**_Stop Elasticsearch container. Change to host port `19200`_**

```bash
docker-compose -f docker-compose.1.yml up -d
```

You can see that compose recognized there was a change in the configuration file and
recreated the `es` container when we ran the `up` command. We used the `-d` option this time
to run the compose services in the background.

We can check to see what's running with `docker-compose ps`:

```bash
docker-compose -f docker-compose.1.yml ps
```

Now, let's query our cluster:

```bash
curl -si localhost:19200/users/User/1
```

All of our data is gone!

```bash
./.d1c
```

**_Volumes Presentation_**

Docker Volumes solve this problem for us!
