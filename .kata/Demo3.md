# Demo 3

You may have noticed earlier that I was passing a `-f` option to Docker Compose and giving it the name of
the file where our services were defined. Docker Compose looks for a file called `docker-compose.yml` by
default so you won't normally have to use that.

```bash
docker-compose up -d && docker-compose logs -f data
```

The first thing Compose does is pull down or build any images it's missing. Then, it starts all of our services
in background mode.

I also ran a `docker-compose logs` command to watch the output from the data generator so we can see when it
completes.

```bash
Ctrl+C
docker-compose logs -f proxy
```

**Discuss wait-for-it.sh**

Now that our sample data has been generated, let's watch our proxy logs and see what happens when we
hit the server.

**_<http://localhost:8080>_**

You can see the access logs from our proxy server show the full DN of the user for each request to both
the API and UI services. This can help you meet enterprise auditing requirements without having to deal
with configuring logging from your individual services, which can be a HUGE time saver!
