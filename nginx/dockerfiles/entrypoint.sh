#!/bin/sh

if [ -n "${HTTPS_REDIRECT_PORT}" -a "${HTTPS_REDIRECT_PORT:0:1}" != ":" ]; then
    HTTPS_REDIRECT_PORT=":${HTTPS_REDIRECT_PORT}"
fi

DOLLAR="$" envsubst < /etc/nginx/nginx.conf.template > /etc/nginx/nginx.conf

case "$1" in
    proxy)
        nginx -g "daemon off;"
        ;;
    *)
        $*
        ;;
esac
